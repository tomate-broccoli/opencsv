package sample;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CsvDateConverter extends AbstractBeanField<T , I> {

    private final String dateFormat;

    public CsvDateConverter(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    protected Object convert(String value) throws CsvDataTypeMismatchException{
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            return sdf.parse(value);
        } catch (ParseException e) {
            throw new CsvDataTypeMismatchException(e.getMessage());
        }
    }

    @Override
    public String[] transmuteBean(T bean) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        String[] result = super.transmuteBean(bean);
        for (int i = 0; i < super.header.length; i++) {
            if (findField(i, bean.getClass()) != null &&
                    findField(i, bean.getClass()).getType().equals(Date.class)) {
                result[i] = new SimpleDateFormat("yyyy-MM-dd").format((Date) findField(i, bean.getClass()).getFieldValue(bean));
            }
        }
        return result;
    }
}
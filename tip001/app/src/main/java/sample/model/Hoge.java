package sample.model;

import lombok.Data;
import java.util.Date;
import java.math.BigDecimal;

@Data
public class Hoge {
  private String name;
  private Short age;
  private Date birthday;
  private BigDecimal deposit;
}